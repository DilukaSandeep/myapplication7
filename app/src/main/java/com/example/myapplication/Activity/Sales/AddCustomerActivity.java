package com.example.myapplication.Activity.Sales;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;

public class AddCustomerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

    }

    public void onClickCancle(View view){
        Intent intent = new Intent(AddCustomerActivity.this, ListCustomerActivity.class);
        startActivity(intent);
    }


}
