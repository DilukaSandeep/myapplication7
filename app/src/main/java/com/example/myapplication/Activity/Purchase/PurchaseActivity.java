package com.example.myapplication.Activity.Purchase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;

public class PurchaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
    }
    public void onClickNewPurchase(View view){
        Intent intent = new Intent(PurchaseActivity.this, NewPurchaseActivity.class);
        startActivity(intent);
    }
    public void onClickReturnPurchase(View view){
        Intent intent = new Intent(PurchaseActivity.this, ReturnPurchaseActivity.class);
        startActivity(intent);
    }
    public void onClickAddSupplier(View view){
        Intent intent = new Intent(PurchaseActivity.this, ListSupplierActivity.class);
        startActivity(intent);
    }
}
