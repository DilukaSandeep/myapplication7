package com.example.myapplication.Activity.Purchase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;

public class AddSupplierActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_supplier);
    }

    public void onClickCancle(View view){
        Intent intent = new Intent(AddSupplierActivity.this, ListSupplierActivity.class);
        startActivity(intent);
    }

}
