package com.example.myapplication.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText email,password;
    private TextView txtSignin;
    private Button btnSIgnup;
    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);

        if(firebaseAuth.getCurrentUser() != null ){
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        btnSIgnup = (Button)findViewById(R.id.btnSignup);
        email = (EditText) findViewById(R.id.edtEmail);
        password = (EditText) findViewById(R.id.edtPassword);
        txtSignin = (TextView) findViewById(R.id.txtSignin);

        btnSIgnup.setOnClickListener(this);
        txtSignin.setOnClickListener(this);
    }

    private void registerUser(){
        String Email = email.getText().toString().trim();
        String Password = password.getText().toString().trim();

        if(TextUtils.isEmpty(Email) || TextUtils.isEmpty(Password)){
                Toast.makeText(SignupActivity.this, "Email or Password is Empty", Toast.LENGTH_SHORT).show();
        }
            progressDialog.setMessage("registering user");
            progressDialog.show();
            //progressDialog.hide();

            firebaseAuth.createUserWithEmailAndPassword(Email, Password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                    Toast.makeText(SignupActivity.this, "Registered Succesful", Toast.LENGTH_SHORT).show();
                                    finish();
                                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                            } else {
                                Toast.makeText(SignupActivity.this, "could not registered", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
    }

    @Override
    public void onClick(View v) {
        if(v == btnSIgnup){
            registerUser();
        }
        if(v == txtSignin){
            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
