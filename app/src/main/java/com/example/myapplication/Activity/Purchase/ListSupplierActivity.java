package com.example.myapplication.Activity.Purchase;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;

public class ListSupplierActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_supplier);

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fabAddSupplier);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListSupplierActivity.this, AddSupplierActivity.class);
                startActivity(intent);
            }
        });
    }
}
