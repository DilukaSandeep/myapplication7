package com.example.myapplication.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.Activity.Account.AccountActivity;
import com.example.myapplication.Activity.Ledger.LedgerActivity;
import com.example.myapplication.Activity.Purchase.PurchaseActivity;
import com.example.myapplication.Activity.Report.ReportActivity;
import com.example.myapplication.Activity.Sales.SalesActivity;
import com.example.myapplication.Activity.Setting.SettingActivity;
import com.example.myapplication.Activity.Stock.StockActivity;
import com.example.myapplication.R;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void onClickAccount(View view){
        Intent intent = new Intent(MainActivity.this, AccountActivity.class);
        startActivity(intent);
    }
    public void onClickLedger(View view){
        Intent intent = new Intent(MainActivity.this, LedgerActivity.class);
        startActivity(intent);
    }
    public void onClickPurchase(View view){
        Intent intent = new Intent(MainActivity.this, PurchaseActivity.class);
        startActivity(intent);
    }
    public void onClickReport(View view){
        Intent intent = new Intent(MainActivity.this, ReportActivity.class);
        startActivity(intent);
    }
    public void onClickSales(View view){
        Intent intent = new Intent(MainActivity.this, SalesActivity.class);
        startActivity(intent);
    }
    public void onClickStock(View view){
        Intent intent = new Intent(MainActivity.this, StockActivity.class);
        startActivity(intent);
    }
    public void onClickSettings(View view){
        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
        startActivity(intent);
    }
}
