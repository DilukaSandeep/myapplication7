package com.example.myapplication.Activity.Sales;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;

public class SalesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);
    }
    public void onClickNewSales(View view){
        Intent intent = new Intent(SalesActivity.this, NewSaleActivity.class);
        startActivity(intent);
    }
    public void onClickReturnSales(View view){
        Intent intent = new Intent(SalesActivity.this, ReturnSaleActivity.class);
        startActivity(intent);
    }
    public void onClickAddCustomer(View view){
        Intent intent = new Intent(SalesActivity.this, ListCustomerActivity.class);
        startActivity(intent);
    }
}
