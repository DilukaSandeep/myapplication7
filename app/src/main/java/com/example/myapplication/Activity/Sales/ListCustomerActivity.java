package com.example.myapplication.Activity.Sales;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;

public class ListCustomerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_customer);

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fabAddCustomer);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListCustomerActivity.this, AddCustomerActivity.class);
                startActivity(intent);
            }
        });
    }
}
